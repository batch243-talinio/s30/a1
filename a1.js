
db.fruits.aggregate([
	{$match: {
      onSale: true
      }},
  {$count: "onSale"}
]);


db.fruits.aggregate([
	{$match: {
      stock: {$gt: 20}
      }},
  {$count: "stock"}
]);


db.fruits.aggregate([
    {$match: {
      onSale: true
    }},
    {$group: {
        _id: "$supplier_id",
        enoughStock: {$gt: 20}
    }}
]);


db.fruits.aggregate([
    {$match: {
      onSale: true
    }},
    {$group: {
        _id: "$supplier_id",
        max_price: {$min: "$price"}
    }},
    {$sort:{max_price: 1}}
]);